import 'package:bmi_calculator/showinfo.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:bmi_calculator/connect.dart';
import 'package:page_transition/page_transition.dart';

import 'addData.dart';
import 'bmi.dart';
import 'calories.dart';
import 'fatpercent.dart';

CollectionReference users = FirebaseFirestore.instance.collection('user');

class historyPage extends StatefulWidget {
  final Future<FirebaseApp> _intitialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _intitialization,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error...');
          } else if (snapshot.connectionState == ConnectionState.done) {
            return historyPage();
          }
          return Text('Loading...');
        });
  }

  historyPage({Key? key}) : super(key: key);

  @override
  _historyState createState() => _historyState();
}

class _historyState extends State<historyPage> {
  CollectionReference users = FirebaseFirestore.instance.collection('user');


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text('ประวัติการบันทึกข้อมูล'),
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
      ),
      drawer: drawer(),

      body: historyTable(),

      //menuBar
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 15.0,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SizedBox(width: 120.0),
            IconButton(
              icon: Icon(
                Icons.person,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: MyApp(),
                    ));
              },
            ),

            IconButton(
              icon: Icon(
                Icons.accessibility_rounded,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: BMIPage(),
                    ));
              },
            ),
            // SizedBox(width: 120.0),
            IconButton(
              icon: Icon(
                Icons.restaurant,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: caloriesPage(),
                    ));
              },
            ),

            IconButton(
              icon: Icon(
                Icons.fitness_center,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: fatPage(),
                    ));
              },
            ),
            IconButton(
              icon: Icon(
                Icons.history,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: historyPage(),
                    ));
              },
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.deepOrange[600],
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.pushReplacement(
              context,
              PageTransition(
                type: PageTransitionType.fade,
                child: addDataPage(),
              ));
        },
      ),
    );
  }
}
