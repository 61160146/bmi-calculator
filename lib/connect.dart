import 'dart:html';

import 'package:bmi_calculator/bmi.dart';
import 'package:bmi_calculator/calories.dart';
import 'package:bmi_calculator/fatpercent.dart';
import 'package:bmi_calculator/history.dart';
import 'package:bmi_calculator/showinfo.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_fortune_wheel/flutter_fortune_wheel.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

import 'addData.dart';
import 'loginpage.dart';

CollectionReference users = FirebaseFirestore.instance.collection('user');

class drawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(children: [
      new Container(
        child: new DrawerHeader(
            child: new CircleAvatar(
          radius: 20.0,
          backgroundImage: NetworkImage(
              'https://data.whicdn.com/images/332249786/original.jpg'),
          backgroundColor: Colors.transparent,
        )),
        color: Colors.white,
      ),
      Container(
          margin: const EdgeInsets.all(15.0),
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            textBaseline: TextBaseline.alphabetic,
            children: [showInfo()],
            // margin: const EdgeInsets.all(20.0),
          )),
      Divider(),
      ListTile(
        leading: Icon(
          Icons.person,
          color: Colors.black,
        ),
        title: Text('บันทึกข้อมูลสุขภาพ'),
        onTap: () {
          Navigator.pushReplacement(
              context,
              PageTransition(
                type: PageTransitionType.rightToLeft,
                child: App(),
              ));
        },
      ),
      ListTile(
        leading: Icon(
          Icons.accessibility_rounded,
          color: Colors.black,
        ),
        title: Text('ดัชนีมวลกาย'),
        onTap: () {
          Navigator.pushReplacement(
              context,
              PageTransition(
                type: PageTransitionType.rightToLeft,
                child: BMIPage(),
              ));
        },
      ),
      ListTile(
        leading: Icon(
          Icons.restaurant,
          color: Colors.black,
        ),
        title: Text('แคลลอรี่ที่ควรได้รับต่อวัน'),
        onTap: () {
          Navigator.pushReplacement(
              context,
              PageTransition(
                type: PageTransitionType.rightToLeft,
                child: caloriesPage(),
              ));
        },
      ),
      ListTile(
        leading: Icon(
          Icons.fitness_center,
          color: Colors.black,
        ),
        title: Text('เปอร์เซ็นต์ไขมันในร่างกาย'),
        onTap: () {
          Navigator.pushReplacement(
              context,
              PageTransition(
                type: PageTransitionType.rightToLeft,
                child: fatPage(),
              ));
        },
      ),
      ListTile(
        leading: Icon(
          Icons.history,
          color: Colors.black,
        ),
        title: Text('ประวัติการบันทึกข้อมูล'),
        onTap: () {
          Navigator.pushReplacement(
              context,
              PageTransition(
                type: PageTransitionType.rightToLeft,
                child: historyPage(),
              ));
        },
      ),
      ListTile(
        leading: Icon(
          Icons.logout,
          color: Colors.black,
        ),
        title: Text('ออกจากระบบ'),
        onTap: () {
          Navigator.pushReplacement(
              context,
              PageTransition(
                type: PageTransitionType.bottomToTop,
                child: LoginWidget(),
              ));
        },
      ),
    ]));
  }
}

class showInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc("5fZuhEfcZVIPtK6ageHm").get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          if (data['gender'] == 'M') {
            return Text("${data['name']}\n เพศ: ชาย  อายุ: ${data['age']} ปี",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, fontFamily: 'THSarabun'));
          } else if (data['gender'] == 'F') {
            return Text("${data['name']}\n เพศ: หญิง  อายุ: ${data['age']} ปี",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, fontFamily: 'THSarabun'));
          }
        }
        return Text("loading");
      },
    );
  }
}

class GetUserName extends StatelessWidget {
  final String documentId;
  GetUserName(this.documentId);
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          return Text("ชื่อ: ${data['name']}");
        }
        return Text("loading");
      },
    );
  }
}

class GetAge extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc("5fZuhEfcZVIPtK6ageHm").get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          return Text("${data['age']}",
              style: TextStyle(fontFamily: 'THSarabun'));
        }
        return Text("loading");
      },
    );
  }
}

class GetHeight extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc("5fZuhEfcZVIPtK6ageHm").get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          return Text("${data['height']}",
              style: TextStyle(fontFamily: 'THSarabun'));
        }

        return Text("loading");
      },
    );
  }
}

class GetWeigh extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc("5fZuhEfcZVIPtK6ageHm").get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          return Text("${data['weigh']}",
              style: TextStyle(fontFamily: 'THSarabun'));
        }
        return Text("loading");
      },
    );
  }
}

class bmiCal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc("5fZuhEfcZVIPtK6ageHm").get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          double bmi =
              data['weigh'] / ((data['height'] / 100) * (data['height'] / 100));
          String result = bmi.toStringAsFixed(2);

          if (bmi < 18.5) {
            return Text(
              "ผอม\n $result",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontFamily: 'THSarabun',
                  fontSize: 20,
                  color: Colors.indigo[900]),
            );
          } else if (bmi >= 18.5 && bmi < 25.0) {
            return Text(
              "สมส่วน\n $result",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontFamily: 'THSarabun',
                  fontSize: 20,
                  color: Colors.green),
            );
          } else if (bmi >= 25.0 && bmi < 30.0) {
            return Text(
              "น้ำหนักเกิน\n $result",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontFamily: 'THSarabun',
                  fontSize: 20,
                  color: Colors.yellow[800]),
            );
          } else if (bmi >= 30.0 && bmi < 35.0) {
            return Text(
              "อ้วนระดับ 1\n $result",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontFamily: 'THSarabun',
                  fontSize: 20,
                  color: Colors.orange[900]),
            );
          } else if (bmi >= 35.0 && bmi < 40.0) {
            return Text(
              "อ้วนระดับ 2\n $result",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontFamily: 'THSarabun',
                  fontSize: 20,
                  color: Colors.pink[600]),
            );
          } else
            return Text(
              "อ้วนระดับ 3 (อันตราย)\n$result",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontFamily: 'THSarabun',
                  fontSize: 20,
                  color: Colors.red[900]),
            );
        }
        return Text("loading");
      },
    );
  }
}

class balance extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc("5fZuhEfcZVIPtK6ageHm").get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          double MinBlance = data['weigh'] - 24.9;
          double MaxBlance = data['weigh'] - 18.5;
          String MinTxt = "${MinBlance.toStringAsFixed(2)}";
          String MaxTxt = "${MaxBlance.toStringAsFixed(2)}";

          return Text(
            "\nน้ำหนักที่เหมาะสม\n $MinTxt ถึง $MaxTxt",
            style: TextStyle(fontFamily: 'THSarabun', fontSize: 14),
          );
        }
        return Text("loading");
      },
    );
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Future<UserCredential> signInWithGoogle() async {
    // Create a new provider
    GoogleAuthProvider googleProvider = GoogleAuthProvider();

    googleProvider
        .addScope('https://www.googleapis.com/auth/contacts.readonly');
    googleProvider.setCustomParameters({'login_hint': 'user@example.com'});

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithPopup(googleProvider);

    // Or use signInWithRedirect
    // return await FirebaseAuth.instance.signInWithRedirect(googleProvider);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      padding: EdgeInsets.all(32),
      children: [
        ElevatedButton(
          onPressed: () async {
            await signInWithGoogle();
          },
          child: Text('Sign In'),
        )
      ],
    ));
  }
}

class name extends StatefulWidget {
  name({Key? key}) : super(key: key);

  @override
  _nameState createState() => _nameState();
}

class _nameState extends State<name> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  late StreamSubscription<User?> _sub;
  @override
  void initState() {
    super.initState();
    _sub = FirebaseAuth.instance.authStateChanges().listen((user) {
      _navigatorKey.currentState!.pushReplacementNamed(
        user != null ? 'home' : 'login',
      );
    });
  }

  @override
  void dispose() {
    _sub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'HelloWorld',
      navigatorKey: _navigatorKey,
      initialRoute:
          FirebaseAuth.instance.currentUser == null ? 'login' : 'home',
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case 'home':
            return MaterialPageRoute(
              settings: settings,
              builder: (_) => App(),
            );
          case 'login':
            return MaterialPageRoute(
              settings: settings,
              builder: (_) => BMIPage(),
            );
          default:
            return MaterialPageRoute(
              settings: settings,
              builder: (_) => App(),
            );
        }
      },
    );
  }
}

class tableBmi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DataTable(
      headingRowColor: MaterialStateColor.resolveWith((states) => Colors.white),
      dataRowColor: MaterialStateColor.resolveWith((states) => Colors.white54),
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'ค่าที่อยู่ในเกณฑ์',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'ค่า BMI',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'ภาวะเสี่ยงต่อโรค',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(Text('ผอม', style: TextStyle(color: Colors.indigo))),
            DataCell(Text('น้อยกว่า 18.5')),
            DataCell(Text('เสี่ยงต่อการได้รับ\nสารอาหารไม่เพียงพอ')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
                Text('น้ำหนักปกติ', style: TextStyle(color: Colors.green))),
            DataCell(Text('18.6 - 24.9')),
            DataCell(Text('สมส่วน เสี่ยงต่อโรคน้อยที่สุด')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('น้ำหนักเกิน',
                style: TextStyle(color: Color(0xFFF9A825)))),
            DataCell(Text('25.0 - 29.9')),
            DataCell(Text('มีควาามเสี่ยงมากกว่าคนปกติ')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('อ้วนระดับ 1',
                style: TextStyle(color: Color(0xFFEF6C00)))),
            DataCell(Text('30.0 - 34.9')),
            DataCell(Text('เสี่ยงต่อการเกิดโรคอ้วนระดับ 1')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('อ้วนระดับ 2', style: TextStyle(color: Colors.pink))),
            DataCell(Text('35.0 - 39.9')),
            DataCell(Text('เสี่ยงต่อการเกิดโรคอ้วนระดับ 2')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('อ้วนระดับ 3', style: TextStyle(color: Colors.red))),
            DataCell(Text('มากกว่า 40.0')),
            DataCell(
                Text('เสี่ยงต่อการเป็นโรคอ้วน\nและโรคต่าง ๆ เป็นอย่างมาก')),
          ],
        ),
      ],
    );
  }
}

class GetActivity extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc("5fZuhEfcZVIPtK6ageHm").get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          double act = data['activity'];

          if (act == 1) {
            return Text("ไม่ออกกำลังกายเลย",
                style: TextStyle(fontFamily: 'THSarabun'));
          } else if (act == 2) {
            return Text(
              "ออกกำลังกายหรือเล่นกีฬาเล็กน้อย\nประมาณอาทิตย์ละ 1-3 วัน",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'THSarabun',
              ),
            );
          } else if (act == 3) {
            return Text(
              "ออกกำลังกายหรือเล่นกีฬาปานกลาง\nประมาณอาทิตย์ละ 3-5 วัน",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'THSarabun',
              ),
            );
          } else if (act == 4) {
            return Text(
              "ออกกำลังกายหรือเล่นกีฬาอย่างหนัก\nประมาณอาทิตย์ละ 6-7 วัน",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'THSarabun',
              ),
            );
          } else
            return Text(
              "ออกกำลังกายหรือเล่นกีฬาอย่างหนักทุกวันเช้าเย็น",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'THSarabun',
              ),
            );
        }
        return Text("loading");
      },
    );
  }
}

class GetGender extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc("5fZuhEfcZVIPtK6ageHm").get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          if (data['gender'] == "M") {
            return Text("ชาย", style: TextStyle(fontFamily: 'THSarabun'));
          } else if (data['gender'] == "F") {
            return Text("หญิง",
                style: TextStyle(
                  fontFamily: 'THSarabun',
                ));
          }
        }
        return Text("loading");
      },
    );
  }
}

class bmrCal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc("5fZuhEfcZVIPtK6ageHm").get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          double bmrMale = 66 +
              (13.7 * data['weigh']) +
              (5 * data['height']) -
              (6.8 * data['age']);
          String resultM = bmrMale.ceil().toInt().toString();
          double bmrFemale = 665 +
              (9.6 * data['weigh']) +
              (1.8 * data['height']) -
              (4.7 * data['age']);
          String result = bmrFemale.ceil().toInt().toString();

          if (data['gender'] == "M") {
            return Text(
              "$resultM kcal",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontFamily: 'THSarabun',
                fontSize: 20,
              ),
            );
          } else if (data['gender'] == 'F')
            return Text(
              "$result kcal",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontFamily: 'THSarabun',
                fontSize: 20,
              ),
            );
        }
        return Text("loading");
      },
    );
  }
}

class tdee extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc("5fZuhEfcZVIPtK6ageHm").get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          double bmrMale = 66 +
              (13.7 * data['weigh']) +
              (5 * data['height']) -
              (6.8 * data['age']);
          double bmrFemale = 665 +
              (9.6 * data['weigh']) +
              (1.8 * data['height']) -
              (4.7 * data['age']);

          if (data['activity'] == 1 && data['gender'] == 'M') {
            double tdee = bmrMale * 1.2;
            String result = tdee.ceil().toInt().toString();
            return Text(
              "$result kcal",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontFamily: 'THSarabun',
                fontSize: 20,
              ),
            );
          } else if (data['activity'] == 1 && data['gender'] == 'F') {
            double tdee = bmrFemale * 1.2;
            String result = tdee.ceil().toInt().toString();
            return Text(
              "$result kcal",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontFamily: 'THSarabun',
                fontSize: 20,
              ),
            );
          }

          if (data['activity'] == 2 && data['gender'] == 'M') {
            double tdee = bmrMale * 1.375;
            String result = tdee.ceil().toInt().toString();
            return Text(
              "$result kcal",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontFamily: 'THSarabun',
                fontSize: 20,
              ),
            );
          } else if (data['activity'] == 2 && data['gender'] == 'F') {
            double tdee = bmrFemale * 1.375;
            String result = tdee.ceil().toInt().toString();
            return Text(
              "$result kcal",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontFamily: 'THSarabun',
                fontSize: 20,
              ),
            );
          }

          if (data['activity'] == 3 && data['gender'] == 'M') {
            double tdee = bmrMale * 1.55;
            String result = tdee.ceil().toInt().toString();
            return Text(
              "$result kcal",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontFamily: 'THSarabun',
                fontSize: 20,
              ),
            );
          } else if (data['activity'] == 3 && data['gender'] == 'F') {
            double tdee = bmrFemale * 1.55;
            String result = tdee.ceil().toInt().toString();
            return Text(
              "$result kcal",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontFamily: 'THSarabun',
                fontSize: 20,
              ),
            );
          }

          if (data['activity'] == 4 && data['gender'] == 'M') {
            double tdee = bmrMale * 1.725;
            String result = tdee.ceil().toInt().toString();
            return Text(
              "$result kcal",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontFamily: 'THSarabun',
                fontSize: 20,
              ),
            );
          } else if (data['activity'] == 4 && data['gender'] == 'F') {
            double tdee = bmrFemale * 1.725;
            String result = tdee.ceil().toInt().toString();
            return Text(
              "$result kcal",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontFamily: 'THSarabun',
                fontSize: 20,
              ),
            );
          }

          if (data['activity'] == 5 && data['gender'] == 'M') {
            double tdee = bmrMale * 1.9;
            String result = tdee.ceil().toInt().toString();
            return Text(
              "$result kcal",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontFamily: 'THSarabun',
                fontSize: 20,
              ),
            );
          } else if (data['activity'] == 5 && data['gender'] == 'F') {
            double tdee = bmrFemale * 1.9;
            String result = tdee.ceil().toInt().toString();
            return Text(
              "$result kcal",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontFamily: 'THSarabun',
                fontSize: 20,
              ),
            );
          }
        }
        return Text("loading");
      },
    );
  }
}

class bodyfat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc("5fZuhEfcZVIPtK6ageHm").get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          double bmi =
              data['weigh'] / ((data['height'] / 100) * (data['height'] / 100));

          if (data['age'] >= 18) {
            if (data['gender'] == 'M') {
              double bodyFat = 1.20 * bmi + 0.23 * data['age'] - 16.2;
              String result = bodyFat.ceil().toInt().toString();
              return SleekCircularSlider(
                  appearance: CircularSliderAppearance(
                      customWidths: CustomSliderWidths(progressBarWidth: 10)),
                  min: 0.0,
                  max: 40.0,
                  initialValue: bodyFat);
            } else if (data['gender'] == 'F') {
              double bodyFat = 1.20 * bmi + 0.23 * data['age'] - 5.4;
              String result = bodyFat.ceil().toInt().toString();
              return SleekCircularSlider(
                  appearance: CircularSliderAppearance(
                      customWidths: CustomSliderWidths(progressBarWidth: 10)),
                  min: 10.0,
                  max: 40.0,
                  initialValue: bodyFat);
            }
          } else if (data['age'] < 18) {
            if (data['gender'] == 'M') {
              double bodyFat = 1.51 * bmi - 0.70 * data['age'] - 2.2;
              String result = bodyFat.ceil().toInt().toString();
              return SleekCircularSlider(
                  appearance: CircularSliderAppearance(
                      customWidths: CustomSliderWidths(progressBarWidth: 10)),
                  min: 10.0,
                  max: 40.0,
                  initialValue: bodyFat);
            } else if (data['gender'] == 'F') {
              double bodyFat = 1.51 * bmi - 0.70 * data['age'] - 1.4;
              String result = bodyFat.ceil().toInt().toString();
              return SleekCircularSlider(
                  appearance: CircularSliderAppearance(
                      customWidths: CustomSliderWidths(progressBarWidth: 10)),
                  min: 10.0,
                  max: 40.0,
                  initialValue: bodyFat);
            }
          }
        }
        return Text("loading");
      },
    );
  }
}

class tableBodyFat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DataTable(
      headingRowColor: MaterialStateColor.resolveWith((states) => Colors.white),
      dataRowColor: MaterialStateColor.resolveWith((states) => Colors.white54),
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'ผู้หญิง',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'ผู้ชาย',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            'ความหมาย',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(Text('น้อยกว่า 13',
                style: TextStyle(color: Colors.deepPurple))),
            DataCell(
                Text('น้อยกว่า 5', style: TextStyle(color: Colors.deepPurple))),
            DataCell(Text('มีไขมันค่อนข้างน้อย เท่าที่จำเป็น')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('14 - 20', style: TextStyle(color: Colors.indigo))),
            DataCell(Text('6 - 13', style: TextStyle(color: Colors.indigo))),
            DataCell(Text('มีไขมันพอประมาณ\n(กลุ่มนักกีฬา)')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('21 - 24', style: TextStyle(color: Colors.green))),
            DataCell(Text('14 - 17', style: TextStyle(color: Colors.green))),
            DataCell(Text('มีไขมันพอประมาณ\n(กลุ่มคนออกกำลังกายเป็นประจำ)')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
                Text('25 - 31', style: TextStyle(color: Color(0xFFF9A825)))),
            DataCell(
                Text('18 - 24', style: TextStyle(color: Color(0xFFF9A825)))),
            DataCell(Text('มีไขมันพอประมาณอยู่เกณฑ์พอดี\n(กลุ่มคนทั่วไป)')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text('มากกว่า 32', style: TextStyle(color: Colors.red))),
            DataCell(Text('มากกว่า 25', style: TextStyle(color: Colors.red))),
            DataCell(Text('มีไขมันมากเกินไป')),
          ],
        ),
      ],
    );
  }
}

class detail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc("5fZuhEfcZVIPtK6ageHm").get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;

          String act = '';
          if (data['activity'] == 1) {
            act = 'ไม่ได้ออกกำลังกายเลย';
          } else if (data['activity'] == 2) {
            act = 'ออกกำลังกายเล็กน้อย\n 1-3 วัน';
          } else if (data['activity'] == 3) {
            act = 'ออกกำลังกายปานกลาง\n 3-5 วัน';
          } else if (data['activity'] == 4) {
            act = '\nออกกำลังกายอย่างหนัก 6-7 วัน';
          } else if (data['activity'] == 5) {
            act = 'ออกกำลังกายอย่างหนักทุกวันเช้าเย็น';
          }
          if (data['gender'] == 'M') {
            return Text(
                "ชื่อผู้ใช้: ${data['name']}\nเพศ: ชาย  อายุ: ${data['age']} ปี\nส่วนสูง: ${data['height']} เซนติเมตร  \nน้ำหนัก: ${data['weigh']} กิโลกรัม\nการออกกำลังกาย: $act",
                textAlign: TextAlign.start,
                style: TextStyle(fontSize: 16, fontFamily: 'THSarabun'));
          } else if (data['gender'] == 'F') {
            return Text(
                "ชื่อผู้ใช้: ${data['name']}\nเพศ: หญิง  อายุ: ${data['age']} ปี\nส่วนสูง: ${data['height']} เซนติเมตร  \nน้ำหนัก: ${data['weigh']} กิโลกรัม\nการออกกำลังกาย: $act",
                textAlign: TextAlign.start,
                style: TextStyle(fontSize: 16, fontFamily: 'THSarabun'));
          }
        }
        return Text("loading");
      },
    );
  }
}

class historyTable extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc("5fZuhEfcZVIPtK6ageHm").get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          return ListView(
            children: [
              DataTable(
                headingRowColor:
                    MaterialStateColor.resolveWith((states) => Colors.white),
                dataRowColor:
                    MaterialStateColor.resolveWith((states) => Colors.white54),
                columns: const <DataColumn>[
                  DataColumn(
                    label: Text(
                      'วันที่',
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'น้ำหนัก',
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'ส่วนสูง',
                    ),
                  ),
                ],
                rows: const <DataRow>[
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        '15/04/2021',
                      )),
                      DataCell(Text(
                        '80',
                      )),
                      DataCell(Text('158')),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        '25/09/2021',
                      )),
                      DataCell(
                          Text('80.5', style: TextStyle(color: Colors.red))),
                      DataCell(Text('-')),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text('29/10/2021')),
                      DataCell(
                          Text('74', style: TextStyle(color: Colors.green))),
                      DataCell(
                          Text('160', style: TextStyle(color: Colors.green))),
                    ],
                  ),
                ],
              )
            ],
          );
        }
        return Text("loading");
      },
    );
  }
}
