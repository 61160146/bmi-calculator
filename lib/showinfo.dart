import 'package:bmi_calculator/bmi.dart';
import 'package:bmi_calculator/fatpercent.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:bmi_calculator/connect.dart';
import 'package:page_transition/page_transition.dart';

import 'addData.dart';
import 'calories.dart';
import 'history.dart';

class App extends StatefulWidget {
  App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final Future<FirebaseApp> _intitialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _intitialization,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error...');
          } else if (snapshot.connectionState == ConnectionState.done) {
            return MyApp();
          }
          return Text('Loading...');
        });
  }
}

CollectionReference users = FirebaseFirestore.instance.collection('user');

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BMI CALCULATOR',
      theme: ThemeData(
        primarySwatch: Colors.teal,
        scaffoldBackgroundColor: Color.fromRGBO(248, 249, 250, 1),
        canvasColor: Colors.white,
      ),
      home: MyHomePage(title: 'BMI CALCULATOR'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  CollectionReference users = FirebaseFirestore.instance.collection('user');
  Future<void> addUser() {
    return users
        .add({
          'name': 'Napatsawan',
          'age': 23,
          'gender': "female",
          'height': 160,
          'weigh': 74,
        })
        .then((value) => print("User Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      //menu
      drawer: drawer(),
      //แสดงหน้า
      body: ListView(children: [
        Container(
            margin: const EdgeInsets.all(15.0),
            padding: const EdgeInsets.all(10.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Image.network(
                    'https://data.whicdn.com/images/332249786/original.jpg',
                    height: 150,
                  ),
                  detail(),
                ])),
        Container(
            margin: const EdgeInsets.all(10.0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Card(
                    child: ListTile(
                      leading: Icon(
                        Icons.accessibility_rounded,
                        color: Colors.indigo[900],
                      ),
                      title: Text('ดัชนีมวลกาย'),
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            PageTransition(
                              type: PageTransitionType.fade,
                              child: BMIPage(),
                            ));
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10.0),
                  ),
                  Card(
                    child: ListTile(
                      leading: Icon(
                        Icons.restaurant,
                        color: Colors.green[900],
                      ),
                      title: Text('แคลอรีที่ควรได้รับต่อวัน'),
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            PageTransition(
                              type: PageTransitionType.fade,
                              child: caloriesPage(),
                            ));
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10.0),
                  ),
                  Card(
                    child: ListTile(
                      leading: Icon(
                        Icons.fitness_center,
                        color: Colors.orange[900],
                      ),
                      title: Text('เปอร์เซ็นต์ไขมันในร่างกาย'),
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            PageTransition(
                              type: PageTransitionType.fade,
                              child: fatPage(),
                            ));
                      },
                    ),
                  ),
                ])),
      ]),

      //menuBar
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 15.0,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SizedBox(width: 120.0),
             IconButton(
              icon: Icon(
                Icons.person,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: MyApp(),
                    ));
              },
            ),

            IconButton(
              icon: Icon(
                Icons.accessibility_rounded,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: BMIPage(),
                    ));
              },
            ),
            // SizedBox(width: 120.0),
            IconButton(
              icon: Icon(
                Icons.restaurant,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: caloriesPage(),
                    ));
              },
            ),
           
            IconButton(
              icon: Icon(
                Icons.fitness_center,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: fatPage(),
                    ));
              },
            ),
            IconButton(
              icon: Icon(
                Icons.history,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: historyPage(),
                    ));
              },
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.deepOrange[600],
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.pushReplacement(
              context,
              PageTransition(
                type: PageTransitionType.fade,
                child: addDataPage(),
              ));
        },
      ),
    );
  }
}
