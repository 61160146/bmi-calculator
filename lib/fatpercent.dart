import 'package:bmi_calculator/showinfo.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:bmi_calculator/connect.dart';
import 'package:page_transition/page_transition.dart';

import 'addData.dart';
import 'bmi.dart';
import 'calories.dart';
import 'history.dart';

CollectionReference users = FirebaseFirestore.instance.collection('user');

class fatPage extends StatefulWidget {
  final Future<FirebaseApp> _intitialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _intitialization,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error...');
          } else if (snapshot.connectionState == ConnectionState.done) {
            return fatPage();
          }
          return Text('Loading...');
        });
  }

  fatPage({Key? key}) : super(key: key);

  @override
  _fatPageState createState() => _fatPageState();
}

class _fatPageState extends State<fatPage> {
  CollectionReference users = FirebaseFirestore.instance.collection('user');

  TextEditingController txtHight = TextEditingController();
  TextEditingController txtWidth = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text('เปอร์เซ็นนต์ไขมันในร่างกาย'),
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
      ),
      drawer: drawer(),
      body: ListView(children: [
        Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            textBaseline: TextBaseline.alphabetic,
            children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    //อายุ
                    Container(
                      margin: const EdgeInsets.all(15.0),
                      padding: const EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(250, 250, 250, 1),
                          border: Border.all(
                              color: Color.fromRGBO(255, 255, 255, 1)),
                          borderRadius:
                              new BorderRadius.all(Radius.circular(10.0)),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(200, 198, 198, 1),
                              spreadRadius: 3,
                              blurRadius: 3,
                              offset:
                                  Offset(0, 2), // changes position of shadow
                            ),
                          ]),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          textBaseline: TextBaseline.alphabetic,
                          children: [
                            Icon(Icons.accessibility_rounded),
                            Text('   อายุ    ',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    fontFamily: 'Palette',
                                    color: Colors.indigo[900])),
                            GetAge(),
                          ]),
                    ),

                    //ส่วนสูง
                    Container(
                      margin: const EdgeInsets.all(15.0),
                      padding: const EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(250, 250, 250, 1),
                          border: Border.all(
                              color: Color.fromRGBO(255, 255, 255, 1)),
                          borderRadius:
                              new BorderRadius.all(Radius.circular(10.0)),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(200, 198, 198, 1),
                              spreadRadius: 3,
                              blurRadius: 3,
                              offset:
                                  Offset(0, 2), // changes position of shadow
                            ),
                          ]),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          textBaseline: TextBaseline.alphabetic,
                          children: [
                            Icon(Icons.height),
                            Text('ส่วนสูง (CM)',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    fontFamily: 'Palette',
                                    color: Colors.indigo[900])),
                            GetHeight(),
                          ]),
                    ),
                    //น้ำหนัก
                    Container(
                        margin: const EdgeInsets.all(15.0),
                        padding: const EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(250, 250, 250, 1),
                            border: Border.all(
                                color: Color.fromRGBO(255, 255, 255, 1)),
                            borderRadius:
                                new BorderRadius.all(Radius.circular(10.0)),
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromRGBO(200, 198, 198, 1),
                                spreadRadius: 3,
                                blurRadius: 3,
                                offset:
                                    Offset(0, 2), // changes position of shadow
                              ),
                            ]),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            textBaseline: TextBaseline.alphabetic,
                            children: [
                              Icon(Icons.add_chart),
                              Text('น้ำหนัก (KG)',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      fontFamily: 'Palette',
                                      color: Colors.indigo[900])),
                              GetWeigh(),
                            ])),
                  ]),
              Container(
                margin: const EdgeInsets.all(20.0),
              ),
              Container(
                margin: const EdgeInsets.only(
                    top: 20, bottom: 20, left: 15, right: 15),
                padding: const EdgeInsets.only(
                    top: 20, bottom: 20, left: 100, right: 100),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  border: Border.all(color: Color.fromRGBO(85, 117, 113, 1)),
                  borderRadius: new BorderRadius.all(Radius.circular(5.0)),
                ),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    textBaseline: TextBaseline.alphabetic,
                    children: [
                      Icon(
                        Icons.emoji_people,
                        color: Colors.yellow[700],
                        size: 40.0,
                      ),
                      Text('เปอร์เซ็นต์ไขมันในร่างกายของคุณ\n',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              fontFamily: 'Palette',
                              color: Colors.indigo[900])),
                      bodyfat(),
                    ]),
              ),
              tableBodyFat(),
              Container(
                margin: const EdgeInsets.only(bottom: 50),
              ),
            ],
          ),
        ),
      ]),

      //menuBar
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 15.0,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SizedBox(width: 120.0),
            IconButton(
              icon: Icon(
                Icons.person,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: MyApp(),
                    ));
              },
            ),
            IconButton(
              icon: Icon(
                Icons.accessibility_rounded,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: BMIPage(),
                    ));
              },
            ),
            // SizedBox(width: 120.0),
            IconButton(
              icon: Icon(
                Icons.restaurant,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: caloriesPage(),
                    ));
              },
            ),
            IconButton(
              icon: Icon(
                Icons.fitness_center,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: fatPage(),
                    ));
              },
            ),
            IconButton(
              icon: Icon(
                Icons.history,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: historyPage(),
                    ));
              },
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.deepOrange[600],
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.pushReplacement(
              context,
              PageTransition(
                type: PageTransitionType.fade,
                child: addDataPage(),
              ));
        },
      ),
    );
  }
}
