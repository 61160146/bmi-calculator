import 'package:bmi_calculator/showinfo.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:bmi_calculator/connect.dart';
import 'package:page_transition/page_transition.dart';

import 'addData.dart';
import 'bmi.dart';
import 'fatpercent.dart';
import 'history.dart';

CollectionReference users = FirebaseFirestore.instance.collection('user');

class caloriesPage extends StatefulWidget {
  final Future<FirebaseApp> _intitialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _intitialization,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error...');
          } else if (snapshot.connectionState == ConnectionState.done) {
            return caloriesPage();
          }
          return Text('Loading...');
        });
  }

  caloriesPage({Key? key}) : super(key: key);

  @override
  _caloriesPageState createState() => _caloriesPageState();
}

class _caloriesPageState extends State<caloriesPage> {
  CollectionReference users = FirebaseFirestore.instance.collection('user');

  TextEditingController txtHight = TextEditingController();
  TextEditingController txtWidth = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text('ค่าแคลอรี่ที่ควรได้รับต่อวัน'),
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
      ),
      drawer: drawer(),
      body: ListView(children: [
        Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            textBaseline: TextBaseline.alphabetic,
            children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    //อายุ
                    Container(
                      margin: const EdgeInsets.all(15.0),
                      padding: const EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(250, 250, 250, 1),
                          border: Border.all(
                              color: Color.fromRGBO(255, 255, 255, 1)),
                          borderRadius:
                              new BorderRadius.all(Radius.circular(10.0)),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(200, 198, 198, 1),
                              spreadRadius: 3,
                              blurRadius: 3,
                              offset:
                                  Offset(0, 2), // changes position of shadow
                            ),
                          ]),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          textBaseline: TextBaseline.alphabetic,
                          children: [
                            Icon(Icons.accessibility_rounded),
                            Text('   อายุ    ',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    fontFamily: 'Palette',
                                    color: Colors.indigo[900])),
                            GetAge(),
                          ]),
                    ),

                    //ส่วนสูง
                    Container(
                      margin: const EdgeInsets.all(15.0),
                      padding: const EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(250, 250, 250, 1),
                          border: Border.all(
                              color: Color.fromRGBO(255, 255, 255, 1)),
                          borderRadius:
                              new BorderRadius.all(Radius.circular(10.0)),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(200, 198, 198, 1),
                              spreadRadius: 3,
                              blurRadius: 3,
                              offset:
                                  Offset(0, 2), // changes position of shadow
                            ),
                          ]),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          textBaseline: TextBaseline.alphabetic,
                          children: [
                            Icon(Icons.height),
                            Text('ส่วนสูง (CM)',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    fontFamily: 'Palette',
                                    color: Colors.indigo[900])),
                            GetHeight(),
                          ]),
                    ),
                    //น้ำหนัก
                    Container(
                      margin: const EdgeInsets.all(15.0),
                      padding: const EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(250, 250, 250, 1),
                          border: Border.all(
                              color: Color.fromRGBO(255, 255, 255, 1)),
                          borderRadius:
                              new BorderRadius.all(Radius.circular(10.0)),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(200, 198, 198, 1),
                              spreadRadius: 3,
                              blurRadius: 3,
                              offset:
                                  Offset(0, 2), // changes position of shadow
                            ),
                          ]),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          textBaseline: TextBaseline.alphabetic,
                          children: [
                            Icon(Icons.add_chart),
                            Text('น้ำหนัก (KG)',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    fontFamily: 'Palette',
                                    color: Colors.indigo[900])),
                            GetWeigh(),
                          ]),
                    ),
                  ]),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    //เพศ
                    Container(
                      margin: const EdgeInsets.all(15.0),
                      padding: const EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(250, 250, 250, 1),
                          border: Border.all(
                              color: Color.fromRGBO(255, 255, 255, 1)),
                          borderRadius:
                              new BorderRadius.all(Radius.circular(10.0)),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(200, 198, 198, 1),
                              spreadRadius: 3,
                              blurRadius: 3,
                              offset:
                                  Offset(0, 2), // changes position of shadow
                            ),
                          ]),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          textBaseline: TextBaseline.alphabetic,
                          children: [
                            Icon(Icons.person),
                            Text('    เพศ    ',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    fontFamily: 'Palette',
                                    color: Colors.indigo[900])),
                            GetGender(),
                          ]),
                    ),

                    //การออกกำลังกาย
                    Container(
                      margin: const EdgeInsets.all(15.0),
                      padding: const EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(250, 250, 250, 1),
                          border: Border.all(
                              color: Color.fromRGBO(255, 255, 255, 1)),
                          borderRadius:
                              new BorderRadius.all(Radius.circular(10.0)),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(200, 198, 198, 1),
                              spreadRadius: 3,
                              blurRadius: 3,
                              offset:
                                  Offset(0, 2), // changes position of shadow
                            ),
                          ]),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          textBaseline: TextBaseline.alphabetic,
                          children: [
                            Icon(Icons.sports_handball),
                            Text('การออกกำลังกาย',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    fontFamily: 'Palette',
                                    color: Colors.indigo[900])),
                            GetActivity(),
                          ]),
                    ),
                  ]),
              Container(
                margin: const EdgeInsets.all(20.0),
              ),
              //bmr
              Container(
                margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.only(
                    top: 50, bottom: 50, left: 100, right: 100),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  border: Border.all(color: Color.fromRGBO(85, 117, 113, 1)),
                  borderRadius: new BorderRadius.all(Radius.circular(5.0)),
                ),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    textBaseline: TextBaseline.alphabetic,
                    children: [
                      Icon(
                        Icons.local_fire_department,
                        color: Colors.orange[900],
                        size: 40.0,
                      ),
                      Text('อัตราการเผาผลาญเริ่มต้น (BMR)',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              fontFamily: 'Palette',
                              color: Colors.indigo[900])),
                      bmrCal(),
                    ]),
              ),
              //tdee
              Container(
                margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.only(
                    top: 50, bottom: 50, left: 100, right: 100),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  border: Border.all(color: Color.fromRGBO(85, 117, 113, 1)),
                  borderRadius: new BorderRadius.all(Radius.circular(5.0)),
                ),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    textBaseline: TextBaseline.alphabetic,
                    children: [
                      Icon(
                        Icons.restaurant_menu,
                        color: Colors.green[700],
                        size: 40.0,
                      ),
                      Text('แคลอรีที่ควรได้รับต่อวัน (TDEE)',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              fontFamily: 'Palette',
                              color: Colors.indigo[900])),
                      tdee(),
                    ]),
              ),
            ],
          ),
        ),
      ]),

      //menuBar
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 15.0,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SizedBox(width: 120.0),
            IconButton(
              icon: Icon(
                Icons.person,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: MyApp(),
                    ));
              },
            ),
            IconButton(
              icon: Icon(
                Icons.accessibility_rounded,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: BMIPage(),
                    ));
              },
            ),
            // SizedBox(width: 120.0),
            IconButton(
              icon: Icon(
                Icons.restaurant,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: caloriesPage(),
                    ));
              },
            ),
            IconButton(
              icon: Icon(
                Icons.fitness_center,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: fatPage(),
                    ));
              },
            ),
            IconButton(
              icon: Icon(
                Icons.history,
              ),
              color: Colors.green[700],
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                      type: PageTransitionType.fade,
                      child: historyPage(),
                    ));
              },
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.deepOrange[600],
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.pushReplacement(
              context,
              PageTransition(
                type: PageTransitionType.fade,
                child: addDataPage(),
              ));
        },
      ),
    );
  }
}
