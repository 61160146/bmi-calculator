import 'package:bmi_calculator/showinfo.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:bmi_calculator/connect.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'bmi.dart';
import 'calories.dart';

CollectionReference users = FirebaseFirestore.instance.collection('user');

class addDataPage extends StatefulWidget {
  @override
  _addDataPageState createState() => _addDataPageState();
}

class _addDataPageState extends State<addDataPage> {
  final _formKey = GlobalKey<FormState>();
  String name = '';
  String gender = 'M';// M, F
  String act = '0' ;
  int age = 0;
  int height=0;
  int weigh=0;

  final _nameController = TextEditingController();
  final _ageController = TextEditingController();
  final _heightController = TextEditingController();
  final _weighController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadProfile();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      _nameController.text = name;
      gender = prefs.getString('gender') ?? 'M';
      age = prefs.getInt('age') ?? 0;
      _ageController.text = '$age';
    });
  }

  Future<void> _saveProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('name', name);
      prefs.setString('gender', gender);
      prefs.setInt('age', age);
    });
  }

  Future<void> addUser() {
    return users
        .add({
           'name': this.name,
          'age': this.age,
          'gender' :this.gender,
          'height':this.height,
          'weigh':this.weigh,
          'activity':int.parse(act)
        })
        .then((value) => print("User Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  Future<void> updateUser() {
    CollectionReference users = FirebaseFirestore.instance.collection('user');
    return users
        .doc('5fZuhEfcZVIPtK6ageHm')
        .update({
          'name': this.name,
          'age': this.age,
          'gender' :this.gender,
          'height':this.height,
          'weigh':this.weigh,
          'activity' : int.parse(act),
        })
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('บันทึกข้อมูลน้ำหนัก'),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.only(top: 10, bottom: 50),
          margin: EdgeInsets.all(15.0),
          child: ListView(
            children: [
              TextFormField(
                controller: _nameController,
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < 5) {
                    return 'กรุณาใส่ข้อมูลชื่อ';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'ชื่อผู้ใช้'),
                onChanged: (value) {
                  setState(() {
                    name = value;
                  });
                },
              ),
              TextFormField(
                controller: _ageController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 0) {
                    return 'กรุณาใส่อายุ ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'อายุ'),
                onChanged: (value) {
                  setState(() {
                    age = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              DropdownButtonFormField(
                value: gender,
                items: [
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.male),
                        SizedBox(width: 8.0),
                        Text('Male')
                      ],
                    ),
                    value: 'M',
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.female),
                        SizedBox(width: 8.0),
                        Text('Female')
                      ],
                    ),
                    value: 'F',
                  ),
                ],
                onChanged: (String? newValue) {
                  setState(() {
                    gender = newValue!;
                  });
                },
              ),
              TextFormField(
                controller: _heightController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 90) {
                    return 'กรุณาใส่ส่วนสูง (cm)';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'ส่วนสูง (cm)'),
                  onChanged: (value) {
                  setState(() {
                    height = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              TextFormField(
                controller: _weighController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || num <= 10) {
                    return 'กรุณาใส่น้ำหนัก (kg)';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'น้ำหนัก (kg)'),
                 onChanged: (value) {
                  setState(() {
                    weigh = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
             
              DropdownButtonFormField(
                value: act,
                items: [
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        SizedBox(width: 8.0),
                        Text('การออกกำลังกาย')
                      ],
                    ),
                    value: '0',
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        SizedBox(width: 8.0),
                        Text('ไม่ได้ออกกำลังกายเลย')
                      ],
                    ),
                    value: '1',
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        SizedBox(width: 8.0),
                        Text('ออกกำลังกายหรือเล่นกีฬาเล็กน้อย ประมาณอาทิตย์ละ 1-3 วัน')
                      ],
                    ),
                    value: '2',
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        SizedBox(width: 8.0),
                        Text('ออกกำลังกายหรือเล่นกีฬาเล็กน้อย ประมาณอาทิตย์ละ 3-5 วัน')
                      ],
                    ),
                    value: '3',
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        SizedBox(width: 8.0),
                        Text('ออกกำลังกายหรือเล่นกีฬาเล็กน้อย ประมาณอาทิตย์ละ 6-7 วัน')
                      ],
                    ),
                    value: '4',
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        SizedBox(width: 8.0),
                        Text('ออกกำลังกายหรือเล่นกีฬาอย่างหนักทุกวันเช้าเย็น')
                      ],
                    ),
                    value: '5',
                  ),
                ],
                onChanged: (String? newValue) {
                  setState(() {
                    act = newValue!;
                  });
                },
              ),
              
              Container(
                margin: const EdgeInsets.all(20.0),
              ),
              Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        _saveProfile();
                        updateUser();
                        Navigator.pushReplacement(
                            context,
                            PageTransition(
                              type: PageTransitionType.rightToLeft,
                              child: App(),
                            ));
                      }
                    },
                    child: const Text('Save')),
                Container(
                  margin: const EdgeInsets.all(5.0),
                ),
                ElevatedButton(
                    onPressed: () {
                      Navigator.pushReplacement(
                          context,
                          PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: App(),
                          ));
                    },
                    child: const Text('Cancle'))
              ]),
            ],
          ),
        ),
      ),
    );
  }

  
  

  
}
